import { StrictMode } from 'react'
import ReactDOM from 'react-dom'

import 'tachyons'
import './assets/css/index.css'

import { App } from './App'

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
) 