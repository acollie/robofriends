import { useState, useEffect } from 'react'

import CardList from './components/CardList'
import SearchBox from './components/SearchBox'
import Scroll from './components/Scroll'

import './assets/css/App.css'

export function App() {
  const [robotsData, setRobotsData] = useState([])
  const [searchField, setSearchField] = useState('')

  const onSearchChange = (e) => {
    setSearchField(e.target.value)
  }

  const filteredRobots = robotsData.filter(robot => {
    return robot.name.toLowerCase().includes(searchField.toLowerCase())
  })

  useEffect(() => {
    fetch('https://jsonplaceholder.cypress.io/users')
      .then(response => response.json())
      .then(robots => setRobotsData(robots))
  }, [])
  return (
      <div className='tc'>
        <h1 className='f1'>RoboFriends</h1>
        <SearchBox searchChange={onSearchChange} />
        <Scroll>
          {
            (robotsData.length === 0) ? (
              <h1>Loading</h1>
            ) : (
              <CardList robots={filteredRobots} />
            )
          }
        </Scroll>
      </div>

  )
}